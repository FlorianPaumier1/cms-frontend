
Cypress.Commands.add("loadUser", () => {
  cy.request("https://back-audiencerun.local.dev.blackcover.fr/adserver_cron/editors/gen_demo_account_data/insert_data/1/168/3")
    .then(response => {
      cy.writeFile('cypress/fixtures/user.json', {
        name: "Test Cypress",
        email: response.body[0].login_email,
        "password": "Demo2019*",
        site: "https://docs.cypress.io",
        domain: "docs.cypress.io"
      })
    })
})

Cypress.Commands.add('login', () => {
  cy.visit('/login')
  cy.fixture('user.json').as('user')
  cy.wait(500)
  cy.get('@user').then(user => {
    cy.get('input[name=login]').type(user.email)
    cy.get('input[name=password]').type(`${user.password}{enter}`)
  })
  // cy.dismissCookieConsent()
  // cy.dismissVerifyEmail()
  // cy.dismissFirstAdvertisingFormat()
})

Cypress.Commands.add('loginDemo', () => {
  cy.visit('/')
  cy.fixture('demo.json').as('demo')
  cy.wait(500)
  cy.get('@demo').then(user => {
    cy.get('input[name=login]').type(user.email)
    cy.get('input[name=password]').type(`${user.password}{enter}`)
  })
  // cy.dismissCookieConsent()
  // cy.dismissVerifyEmail()
  // cy.dismissFirstAdvertisingFormat()
})

Cypress.Commands.add('logout', () => {
  cy.wait(500)
  cy.get('#user-menu-link').click()
  cy.get('.btn-sign-out')
    .contains('Sign out')
    .click()
  cy.url().should('include', '/login')
})

Cypress.Commands.add('dismissCookieConsent', () => {
  cy.wait(500)
  cy.get('html').then($element => {
    if ($element.find('.cc-dismiss').is(':visible')) {
      cy.get('.cc-dismiss').click({ force: true })
    }
  })
})

Cypress.Commands.add('dismissVerifyEmail', () => {
  cy.wait(1000)
  cy.get('html').then($element => {
    if ($element.find('#email-verify-popup').is(':visible')) {
      cy.get('#email-verify-popup-confirm').click({ force: true })
    }
  })
})

Cypress.Commands.add('dismissFirstAdvertisingFormat', () => {
  cy.wait(1000)
  cy.get('html').then($element => {
    if ($element.find('#new-format-popup').is(':visible')) {
      cy.get('#new-format-popup')
        .get('#anymore')
        .check()
      cy.get('#format-popup')
        .get('a.popup__link')
        .click({ force: true })
    }

    if ($element.find('#billing-popup').is(':visible')) {
      cy.get('#billing-popup')
        .get('#confirm-billing')
        .check()
      cy.get('#billing-popup')
        .get('a.popup__link')
        .click({ force: true })
    }
  })
})

Cypress.Commands.add('goToDashboard', () => {
  cy.get('#dashboard-aside-link')
    .first()
    .click({ force: true })
})

Cypress.Commands.add('goToBilling', () => {
  cy.get('#billing-aside-link')
    .first()
    .click({ force: true })
  cy.wait(500)
})

Cypress.Commands.add('goToSponsorship', () => {
  cy.get('#sponsorship-aside-link')
    .first()
    .click({ force: true })
})

Cypress.Commands.add('createFormat', (format) => {
  const { param, selector } = format
  cy.get(selector).parent().click({ multiple: true, force: true })
  cy.get('.pop-options-btn').children('div').siblings().children('.ar-general-btn').click({ multiple: true, force: true })
  cy.wait(1000)
  cy.url().should('contain', 'new-format?type=' + param)

  if (param === 'all') {
    cy.get('input#format-type-autre').click({ multiple: true, force: true })
    cy.wait(500)
  }

  cy.get('.card-format.format').within(() => {
    cy.get('button.ar-general-btn').click({ multiple: true, force: true })
  })
  cy.wait(5000)

  cy.get('#new-config-popup .ar-general-btn.popup__link-btn').click({ multiple: true, force: true })
  cy.wait(500)

  cy.url().should('contain', 'config?type=' + param)
  cy.get('.box-body.box-code').should('exist').should('be.visible')
})

Cypress.Commands.add('goToProfile', () => {
  cy.visit('/setting')
  cy.wait(800)
  cy.dismissVerifyEmail()
})

Cypress.Commands.add('checkCopyrightDateLogin', () => {
  cy.visit('/login')
  cy.wait(1000)
  let date = new Date().getFullYear()
  cy.get('.authentication__sign').should('contain', `© ${date} AudienceRun. All rights reserved.`)
})

Cypress.Commands.add('changeUserCountry', countryCode => {
  cy.goToProfile()
  cy.get('#country').select(countryCode)
  cy.get('#profile')
    .find('input[type="submit"]')
    .click({ force: true })
  cy.wait(500)
  // TODO fix cypressError: Timed out retrying: Expected to find element: '.board-response', but never found it.
  // cy.get('.board-response').should('contain', 'Profile successfully modified')
})
