/// <reference types="Cypress" />

context('Authentification', () => {

  beforeEach(() => {
    cy.visit('/login')
    // cy.title().should('include', 'Audiencerun')
    cy.fixture('user.json').as('user')
  })


  it('Login - requires login', () => {
    cy.get('@user').then(user => {
      cy.get('input[name=password]').type(user.password + '{enter}')
    })
    cy.url().should('include', '/login')
  })

  it('Login - requires password', () => {
    cy.get('@user').then(user => {
      cy.get('input[name=login]').type(user.email + '{enter}')
    })
    cy.url().should('include', '/login')
  })

  it('Login - Authentification ok', () => {
    cy.get('@user').then(user => {
      cy.get('input[name=login]').type(user.email)
      cy.get('input[name=password]').type(user.password + '{enter}')
    })
  })

  it('Logout - Deconnection', () => {
    cy.login()
    cy.wait(1000)

    cy.get('header')
        .get(".profile-button-icon")
        .click()
        .get('.logout')
      .contains('Logout')
      .click()
    cy.url().should('include', '/')
  })

})
